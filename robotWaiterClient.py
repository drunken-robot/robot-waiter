import socket
import sys

s = socket.socket()
host = socket.gethostname()
port = 8118

s.connect((host, port))
connected = s.recv(1024)

print("CLIENT: Connected to [%s] on port %s" % (host, port))

keepConnection = True;
while (keepConnection):
	val = input("Enter a table number that has entered an order [-1 to quit]: ")
	if(val == -1):
		print('Quitting')
		s.send(bytes(-1))
		keepConnection = False
	else:
		s.send(bytes(val))
	