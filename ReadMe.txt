Installation and Instructions

Needed:
	2 Arduino Uno/RedBoards
	2 740 resisters
	2 IR LEDs
	Assorted wires to wire the LEDs
	1 RaspberyPi
		PyCreate installed
		GPIO Library installed
	1 Drink Sensing Box

Set Up the 2 Arduinos (Identically)
	Connect Digital pin 3 to the resistor. 
	Connect the resistor to the positive side of the LED. 
	Connect the negative side of the LED to the ground pin.

Set up the RaspberryPi GPIO with the drink box
	Connect GPIO pin 23(BCM) to one of the wires from the drink sensing box.		  
        Connect the other wire from the box to a ground GPIO pin.

Set Up and Run
	Install the files by unzipping the files in a directory on the pi.
	Run sudo python fancierFSM.py. Sudo is need to use the GPIO.

IR Bytes transmitted from arduinos: Currently programmed 135 is "home" or the bar, and 136 is a table.