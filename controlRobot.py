from pycreate import create
import socket
import sys
import Queue
import threading
from time import sleep

MAX_QUEUE_SIZE = 20

tableQueue = Queue.Queue(MAX_QUEUE_SIZE)

def watchWebsocket():
	s = socket.socket()
	host = socket.gethostname() # Change hostname to expose to an external port
	port = 8118

	s.bind((host, port))
	print("SERVER: running on %s, %s" % (host, port))
	s.listen(0)

	while(True):
		(client_socket, client_addr) = s.accept()
		print("SERVER: Connection recieved from [%s,%s]" % client_addr)
		client_socket.send("True")
		connectionAlive = True
		while(connectionAlive):
			inp = client_socket.recv(1024)
			if not inp:
				connectionAlive = False
 				client_socket.close()
			if(int(inp) == -1):
				connectionAlive = False
 				client_socket.close()
			else:
				# add input to the queue
				tableQueue.put(int(inp))
				print("SERVER: Added table [%s] to queue" % inp)

	s.close()

def controlRobot():
	while(True):
		if(tableQueue.qsize() == 0):
			sleep(1)
		else:
			print("CONTROL: Read %d from Queue" % tableQueue.get())
			sleep(2)

#watchWebsocket()
webserverThread = threading.Thread(target=watchWebsocket)
robotControlThread = threading.Thread(target=controlRobot)

print("Starting Server Thread")
webserverThread.start()
print("Starting Robot Control Thread")
robotControlThread.start()
