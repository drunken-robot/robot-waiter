#python skeleton
from pycreate import create
from time import sleep
#from drink_detection import drinkWasRemoved


robotState = "HOME"
robot = create.Create("/dev/cu.usbserial")

def main():

	while True:
		# if the robot is on the home base and charging
		charge_state = robot.getSensor("CHARGING_STATE")
		decideWhatToDo()

	
	
def decideWhatToDo():

	degree_turned = 0

	
	#and the drink was added to the robot
	# and !drinkWasRemoved
	if(robotState=="HOME"):
		#change the state to be moving to the center of the room
		robotState="MovingToCenter"
		navigateMiddle(robot)

	#if the roobot state is in the center of the room, chage the state to be moving to a table
	elif(robotState == "InCenter"):
		robotState = "MovingToTable"
		degree_turned = navtoTable(robot, tableNum)

	#if state is moving to a table and it has arrived change state to be hading home
	elif(robotState == "AtTable"):
		robotState = "GoingHome"
		
		# when arrieved at table, wait until the drink as been removed
		# while(!drinkWasRemoved):
		# 		sleep(.5)
		# once the drink has been removed return to homebase
		headHome(robot, degree_turned)


#function to get the robot to go to the right table
def navtoTable(tableNum):
	
	#Angle sensor returs angkle change since last time it was called. Call it now to clear it out.
	degree_turned = robot.getSensor("ANGLE")
	
	#if the tabe is 1 or 2 rotate CCW until we see the correct IR signal
	# tableNum['num'] = 1 or tableNum['num'] = 2
	if(True):
		#begin rotating
		robot.go(0,10)
		#keep checking the IR untill its good then stop the robot and get the ange it moved
		while True:
			irByte = robot.getSensor("IR_BYTE")
			irByte = tableNum['ir']
			if(True):
				robot.stop()
				#get the degree we turned
				#keep track of the ange turned and return it later
				degree_turned = robot.getSensor("ANGLE")
				break
	#if the table is 3 or 4 rotate CW until we see the correct IR signal
	elif(tableNum['num'] == 3 or tableNum['num'] == 4):
		#begin rotating
		robot.go(0,-10)
		#keep checking the IR untill its good then stop the robot and get the ange it moved
		while True:
			irByte = robot.getSensor("IR_BYTE")
			if(irByte == tableNum['ir']):
				robot.stop()
				#get the degree we turned
				#keep track of the ange turned and return it later
				degree_turned = robot.getSensor("ANGLE")
				break

	#both if and elif should hit this code once they complete
	#go foward until we hit the table
	robot.go(10,0)
	#keep looking for a bump
	while True:
		leftBumper = robot.getSensor("BUMPS_AND_WHEEL_DROPS")[3]
		rightBumper = robot.getSensor("BUMPS_AND_WHEEL_DROPS")[4]

		#either the left, the right, or both bump sensors were pressed 
		if(leftBumper ==1 or rightBumper == 1):
			robot.stop()
			robotState="AtTable"
			break
	return degree_turned


#function to get the robo to go to the center of the room
def navigateMiddle():
	#back up out of the charging base
	robot.go(-10,0)
	robot.waitDistance(-10)

	#rotate 180 to be facing the center
	robot.go(0,10)
	robot.waitAngle(180)

	#drive to the middle
	robot.go(10,0)
	robot.waitDistance(190)

	#change the state
	robotState="InCenter"
	decideWhatToDo()


#function to return to the homebase
def headHome(degree_turned):
	
	#rotate 180 degrees to face the center of the room
	robot.go(0,30)
	robot.waitAngle(180)

	#go back that distance to the center of the room

	#rotate to face the base
	robot.go(0,-10)
	robot.waitAngle(-1 * degree_turned)

	#drive foward to get closer to the home base before deciding looking for it using IR
	robot.go(20, 0)
	robot.waitDistance(100)

	#look for the base
	seekDock()

	#when the charging state is charging then set the state to be home
	#if the chrge_state is 
		# 1 Reconditioning Charging
		# 2 Full Charging
		# 3 Trickle Charging

	#then it is on the base
	while True:
		charge_state = robot.getSensor("CHARGING_STATE")
		if(charge_state == 1 or charge_state == 2 or charge_state == 3):
			robotState = "HOME"
			decideWhatToDo()
			break


#########
#Call our main function
main()
##########