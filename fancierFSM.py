##############################################################################################################
##
##  Authors:        Peter Zorzonello & Tyler Lemieux
##  Last Updated:   5/8/16
##
##  Description:    A working demo using the Mac (not Pi) that shows the robot going to the same table,
##                  waiting five seconds, and then going back to its starting position. Repeates indefinitely.
##
##############################################################################################################

from time import sleep
from pycreate import create

#inport the function to detect if the drink is on the platform
from drink_detection import drinkWasRemoved

#the robot 
#/dev/ttyUSB0
#/dev/cu.usbserial
robot = create.Create("/dev/ttyUSB0")

# FUTURE TODO: use the table mappings to implement the queue.
# Provides the ability to service mutiple tables.
tableMapping = {1: 136, 2: 137, 3: 0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0, 10:0}



# What a State class looks like
class State(object):
    def run(_self,degree_turned, _distance_travled):
        self.degree_turned = _degree_turned
        self.distance_travled = _distance_travled
        # do what this state should do
        pass
        # "sense"
        # --then--
        # return "input" for computing next state as needed
        return None

    def next_state(self, input):
        # return the next state
        pass

    def read_sensor(self):
        inp = input("fake_sensor [1/0]:")
        return inp

#The state in whic the robot is heading back to the center of the room and then to the home base
#The state is changed to be Home when it bumps into the sensor
class HeadHome(State):
    def run(self, _degree_turned, _distance_travled):
        self.degree_turned = _degree_turned
        self.distance_travled = _distance_travled
        print("HeadHome")
        
        #rotate 180 degrees to face the center of the room
        # robot.go(0,30)
        # sleep(6)
        #robot.waitAngle(180)

        #go back that distance to the center of the room
        #travles backwards
        robot.go(-10,0)
        print ("Going home distance to travle: %s" %(self.distance_travled))
        sleep(self.distance_travled/10)
        robot.stop()

        #rotate the complete the circle and face away from the base
        print("Going home angle to rotate: %d" %self.degree_turned )
        print(self.degree_turned/10)
        robot.go(0,10)
        sleep( (360-(self.degree_turned % 360)) /10)
        #robot.waitAngle(-1 * degree_turned)
        robot.stop()

        #get the robot to rotate 180 degrees so it is facing the base
        robot.go(0,10)
        sleep(18)

        #drive foward to get closer to the home base before looking for it using IR
        print ("heading back to the base")
        robot.go(20, 0)
        sleep(8)
        #robot.waitDistance(100)

        #look for the base
        print("Looking for IR 135")
        robot.go(0,10)
        while True:
            irByte = robot.getSensor("IR_BYTE") 
            if(irByte == 135):
                robot.stop()
                print ("Found base")
                break
        
        #go foward until you hit the base
        robot.go(10)
        print("moving directly foward until bumps")
        while True:
            leftBumper = robot.getSensor("BUMPS_AND_WHEEL_DROPS")[3]
            rightBumper = robot.getSensor("BUMPS_AND_WHEEL_DROPS")[4]

            #either the left, the right, or both bump sensors were pressed 
            if(leftBumper ==1 or rightBumper == 1):
                robot.stop()
                print ("bumped")
                break

        return None

    #returns the next state the robot is in.
    def next_state(self, data):
        return {"state": "Home", "degree": None, "distance": None}

                
#When the robot is sitting at the Bar it is in this state.
#If the drink is removed it should move to the next state
class Home(State):
    #what the robot does when sitting at the bar. The robot should do nothing
    def run(self, _degree_turned, _distance_travled):
        self.degree_turned = _degree_turned
        self.distance_travled = _distance_travled
        print("Home")
        return None

    #how the robot decides to change its state
    def next_state(self, data):
        #drink was added to the robot
        # not drinkWasRemoved
        if(not drinkWasRemoved()):
            _state = "MovingToCenter"
        else:
            _state = "Home"

        return {"state": _state, "degree": None, "distance": None}

#The state of the robot in whic it is moving away from the base and towards the center of the room
class MovingToCenter(State):
    #the steps the robot takes when it is in this state
    def run(self,_degree_turned, _distance_travled):
        print("MovingToCenter")
        self.degree_turned = _degree_turned
        self.distance_travled = _distance_travled
        
        #back up out of the charging base
        robot.go(-10,0)
        sleep(1)
        #robot.waitDistance(-10)

        #rotate 180 to be facing the center
        robot.go(0,10)
        sleep(18)
        #robot.waitAngle(180)

        #drive to the middle
        robot.go(10,0)
        sleep(20)
        #robot.waitDistance(200)
        robot.stop()

        #change the state
        return "InCenter"

    def next_state(self, data):
        return {"state": "InCenter", "degree": None, "distance": None}

# This state is the state that controlls the robot when it is at the table.
# When the drink is removed it should then head back to the home base
# Right now it just waits 5 seconds then heads back
class AtTable(State):
    def run(self, _degree_turned, _distance_travled):
        self.degree_turned = _degree_turned
        self.distance_travled = _distance_travled
        print("AtTable")
        
        #do nothing
        sleep(5)

    def next_state(self, data):
        #if the frink was removed return to home base
        #TODO: if the drink was removed head back, else stay at the table
        if(drinkWasRemoved()):
            _state = "HeadHome"
        else:
            _state = "AtTable"

        return {"state": _state, "degree": None, "distance": None}

# This class controlls the robot when it is in the center 
# It spins left or right and stopes when the proper IR is detected
# It then moves foward until it hits a table and then stops
class InCenter(State):
    def run(self, _degree_turned, _distance_travled):
        self.degree_turned = _degree_turned
        self.distance_travled = _distance_travled

        print("InCenter")
        #Angle sensor returs angkle change since last time it was called. Call it now to clear it out.
        self.degree_turned = robot.getSensor("ANGLE")
        

        # TODO: change this to use the table mapping
        #
        #
        #
        #
        #


        #if the tabe is 1 or 2 rotate CCW until we see the correct IR signal
        # tableNum['num'] == 1 or tableNum['num'] == 2
        #this code will always run beacue in this demo we are only looking for one table
        if(True):
            #begin rotating
            robot.go(0,10)
            #keep checking the IR untill its good then stop the robot and get the ange it moved
            while True:
                irByte = robot.getSensor("IR_BYTE")
                
                #hard coded to look for one table
                #TODO: change this to look for the proper IR for the table we are looking for
                if(irByte == 136):
                    robot.stop()
                    #get the degree we turned
                    #keep track of the ange turned and return it later
                    self.degree_turned = robot.getSensor("ANGLE")
                    print ("degree_turned: %s" %(self.degree_turned))
                    break
                    # return None
        
        #this code will not run for this demo
        #if the table is 3 or 4 rotate CW until we see the correct IR signal
        #FUTURE TODO: Change this so that mutiple tables can be services
        elif(tableNum['num'] == 3 or tableNum['num'] == 4):
            #begin rotating
            robot.go(0,-10)
            #keep checking the IR untill its good then stop the robot and get the ange it moved
            while True:
                irByte = robot.getSensor("IR_BYTE")
                # irByte == tableNum['ir']
                if(rByte == 137):
                    robot.stop()
                    #get the degree we turned
                    #keep track of the ange turned and return it later
                    self.degree_turned = robot.getSensor("ANGLE")
                    break
                    # return None
        
        #both if and elif should hit this code once they complete
        
        #like ANGLE reset distance so it starts tracking here. DISTANCE uses mm so devide by 10 to get cm
        self.distance_travled = (robot.getSensor("DISTANCE")/10)

        robot.go(10,0)
        #keep looking for a bump
        while True:
            leftBumper = robot.getSensor("BUMPS_AND_WHEEL_DROPS")[3]
            rightBumper = robot.getSensor("BUMPS_AND_WHEEL_DROPS")[4]

            #either the left, the right, or both bump sensors were pressed 
            if(leftBumper ==1 or rightBumper == 1):
                robot.stop()
                self.distance_travled = (robot.getSensor("DISTANCE")/10)
                print("distance_travled: %d" %(self.distance_travled))
                # robotState="AtTable"
                break
        return self.degree_turned

    def next_state(self, data):
        return {"state": "AtTable", "degree": self.degree_turned, "distance": self.distance_travled}

#The main function. Called when the program runs.
def main():
    known_states = {"HeadHome":HeadHome(), "Home":Home(), "MovingToCenter":MovingToCenter(), "InCenter":InCenter(), "AtTable":AtTable() }
    
    current_state = known_states["Home"]
    degree_turned = 0
    distance_travled = 0    
    
    while(True):
        result = current_state.run(degree_turned, distance_travled)
        data = current_state.next_state(result)
        print data
        next_state_name = data["state"]
        if(data["degree"] != None):
            degree_turned = data["degree"]
        if(data["distance"] != None):
            distance_travled = data["distance"]
#        next_state_name = current_state.next_state(result)
        current_state = known_states[next_state_name]

    current_state.run()

if __name__ == '__main__':
    main()
