####################################################################
##
##	Authors:		Peter Zorzonello & Tyler Lemieux
##	Last Update:	5/8/16
##
##	Description:	Detectis if the drink is on the wooden platform	
##
####################################################################

import RPi.GPIO as GPIO


#This function detects if the drink is on the platform or not
#Returns false if the drink is on the platform
def drinkWasRemoved():

	GPIO.setmode(GPIO.BCM)

	#GPIO pin 23 has a pull down resistor in it. Use this pin instead
	GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)


	
	#if the inut is HIGH then print to the screen
	#and return that the drink is on the platform
	if (GPIO.input(23) == 1):
		print("Drink on the platform.")
		return False
	
	#print that the drink was removed
	#return true,the drink was removed
	else:
		print("The drink was removed.")
		return True
